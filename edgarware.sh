#!/bin/bash

    local OPTIND;
    function usage () 
    { 
        cat
    }  <<EOF
usage: $0 options

OPTIONS:
    -h Show this message
    -a Scan with all signatures
    -s Scan for web shells
    -E Scan for encoded files
    -e Scan for eitest malware
    -r Scan for redirects
    -b Scan for botnets
    -M Scan for mailers
    -S Scan for script kiddie crap
    -u Username
EOF

    while getopts "absSEerMhu:" OPTION; do
        case $OPTION in 
            s)
                shellsig=" -d ${HOME}/malware_signatures/webshell.yar"
            ;;
            E)
                encodingsig=" -d ${HOME}/malware_signatures/encoding.yar"
            ;;
            e)
                eitestsig=" -d ${HOME}/malware_signatures/eitest.yar"
            ;;
            r)
                redirectsig=" -d ${HOME}/malware_signatures/redirects.yar"
            ;;
            b)
                botnetsig=" -d ${HOME}/malware_signatures/botnet.yar"
            ;;
            M)
                mailersig=" -d ${HOME}/malware_signatures/mailers.yar"
            ;;
            S)
                skiddiesig=" -d ${HOME}/malware_signatures/skiddie_crap.yar"
            ;;
            a)
                shellsig=" -d ${HOME}/malware_signatures/webshell.yar";
                encodingsig=" -d ${HOME}/malware_signatures/encoding.yar";
                eitestsig=" -d ${HOME}/malware_signatures/eitest.yar";
                redirectsig=" -d ${HOME}/malware_signatures/redirects.yar";
                botnetsig=" -d ${HOME}/malware_signatures/botnet.yar";
                mailersig=" -d ${HOME}/malware_signatures/mailers.yar";
                skiddiesig=" -d ${HOME}/malware_signatures/skiddie_crap.yar"
            ;;
            u)
                theuser=${OPTARG}
            ;;
            h)
                usage;
                exit
            ;;
            *)
                usage;
                exit
            ;;
        esac;
    done;
    if (( $(clamscan --version | cut -d/ -f1 | cut -d. -f2) < 98 )); then
        printf "The version of clamscan installed is too old to use yara signatures! Terminating...\n";
        exit;
    fi;
    datevar=$(date +"%Y%m%d");
    timevar=$(date +"%H.%M.%S");
    iamhimwhoiscallediam=$(whoami);
    test -d "${HOME}/malware_signatures" && sigdirexists="1" || sigdirexists="0";
    test -d "${HOME}/malware_scans/${datevar}" && scandirexists="1" || scandirexists="0";
    test -d "${HOME}/malware_scans" && scandirparentexists="1" || scandirparentexists="0";
    if [[ ${sigdirexists} == 0 ]]; then
        git clone https://gitlab.com/firemandave392/malware_signatures --quiet;
    else
        folderdate=$(date -d $(stat -c %z ${HOME}/malware_signatures/ | awk '{ print $1 }') +%s);
        killit=$(date -d $(date +%F --date="7 days ago") +%s );
        if [ ${killit} -ge ${folderdate} ]; then
            printf "Signatures out of date, deleting...\n";
            rm --preserve-root -rf ${HOME}/malware_signatures;
            git clone https://gitlab.com/firemandave392/malware_signatures --quiet;
        fi;
    fi;
    if [[ ${scandirparentexists} == 0 ]]; then
        mkdir ${HOME}/malware_scans;
    fi;
    if [[ ${scandirexists} == 0 ]]; then
        mkdir ${HOME}/malware_scans/${datevar};
    fi;
    theirdir=$(getent passwd ${theuser} | cut -d: -f6);
    sudo clamscan -ir -l ${HOME}/malware_scans/${datevar}/${theuser}.malware.${timevar}.txt --exclude-dir={.trash,.a2-optimized,access-logs,bin,cache,.cl.selector,etc,.htpasswds,lscache,share,.softaculous,.ssh,ssl,.subaccounts,tmp,turbocache,.wp-cli,www,srbackups,a2quarantine,lib,mail,perl5,.cpanel,.cphorde} ${shellsig} ${encodingsig} ${eitestsig} ${magentosig} ${redirectsig} ${botnetsig} ${mailsersig} ${theirdir};
    sudo chown -R ${iamhimwhoiscallediam}. ${HOME}/malware_scans/
